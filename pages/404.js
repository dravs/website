import Link from 'next/link'
import { useEffect } from 'react'
import { useRouter } from 'next/router'
import Head from "next/head";
import styles from '../styles/NotFound.module.css'

const NotFound = () => {
    const router = useRouter()

    useEffect(() => {
        setTimeout(() => {
            router.push('/')
        }, 5000)
    })

    return (
        <div className={styles.notFoundContainer}>
            <Head>
                <title>Damian Prorok | Software Developer | Not Found</title>
                <meta name="description" content="Not found page"/>
            </Head>
            <div className={styles.notFound}>
            <h1>OOPS!</h1>
            <h2>404 - The Page can&apos;t be found</h2>
            <Link href="/"><a>Homepage</a></Link>
            </div>
        </div>
    );
}

export default NotFound;