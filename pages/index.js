import Head from 'next/head'
import {useEffect} from "react";
import Contact from "../components/home/Contact";
import Experience from "../components/home/Experience";
import Skills from "../components/home/Skills";
import AboutMe from "../components/home/AboutMe";
import Hello from "../components/home/Hello";
import BGImage from "../components/home/BGImage";

const Home = () => {
    useEffect(() => {
        window.history.scrollRestoration = 'manual'
    }, []);

    return (
        <div className="wrapper">
            <Head>
                <title>Damian Prorok | Software Developer | Home</title>
                <meta name="description" content="Home page"/>
            </Head>
            <BGImage/>
            <Hello/>
            <AboutMe/>
            <Skills/>
            <Experience/>
            <Contact/>
        </div>
    );
};
Home.displayName = 'Home';
export default Home;