import Head from "next/head";
import ComingSoon from "../components/ComingSoon";

const Software = () => {
    return (
        <div>
        <Head>
                <title>Damian Prorok | Software Developer | Software</title>
                <meta name="description" content="Software page"/>
            </Head>
            <ComingSoon pagename="Software"/>
        </div>
    );
};

export default Software;