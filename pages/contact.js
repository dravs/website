import Head from "next/head";
import ContactForm from "../components/ContactForm";
import styles from "../styles/Contact.module.css";
import LinkedIn from "../public/svg/linkedin.svg";
import Email from "../public/svg/email.svg";

const Contact = () => {
    const Discord = "images/discord.jpeg";
    return (
        <div>
            <Head>
                <title>Damian Prorok | Software Developer | Contact</title>
                <meta name="description" content="Contact page"/>
            </Head>
            <section className={styles.contactContainer}>
                <h1>Contact</h1>
                <p>Got a question, proposal or suggestion, or just want to say hello? I&apos;ll be happy to answer any questions about my projects.</p>
                <div className={styles.contactList}>
                    <div><LinkedIn width={32} height={32} fill={'black'}/><p>damian-prorok</p></div>
                    <div><Email width={34} height={34} fill={'black'}/><p>prorokdamian@gmail.com</p></div>
                    <div><img src={Discord} alt="Discord" className={styles.discordImg}/><p>dravs#0514</p></div>
                </div>
                <p>Go ahead, You can contact me directly or thru the form.</p>
                <ContactForm/>
            </section>
        </div>
    );
};

export default Contact;