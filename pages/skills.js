import Head from "next/head";
import styles from "../styles/Skills.module.css";

const Skills = () => {
    return (
        <div>
            <Head>
                <title>Damian Prorok | Software Developer | Skills</title>
                <meta name="description" content="My skills page"/>
            </Head>
            <section className={styles.skillsContainer}>
                <h2>Skills</h2>
                <p>The main area of my expertise is back-end development, with REST API on the front of it.</p>
                <div className={styles.skills}>
                    <div className={styles.skillsText}>
                        <p>Java is my first programming language, currently with 4 years of experience as a developer in it. Since beginning my journey
                            I&apos;ve worked with frameworks like Dropwizard, Spring, Hibernate and tools like Kubernetes, Docker, Jenkins. I was working on software, where we used hexagonal architecture and Domain-Driven Design. That experience learned me to organize code better.</p>
                        <p>I&apos;m a big fan and user of Linux since 2014. I&apos;ve used almost all possible distributions, but my favorite one is Arch.
                            That knowledge helped me built, my own working k3s (lighter version of k8s) cluster on Raspberry PI 4. (e.g this website is hosted there).
                            </p>
                        <p>Front end is not my strongest aspect, so this website allows me to experiment more with the tools need to build it. I&apos;ve decided to do it from scratch to better understanding technology.
                            In the past, I&apos;ve using open-source CMS like WordPress, Drupal.</p>
                        <p>I’m constantly trying to learn new technologies and concepts. At someday, I want have enough skill of using VIM, to be able use it as main IDE, as a true developer doing :)</p>
                        <p><small><small>:wq</small></small></p>
                    </div>
                    <div className={styles.chartContainer}>
                        <div className={styles.chart}>
                            <span>Java</span>
                            <footer>
                                <div className={styles.chartBar}>
                                    <style jsx>{`
                                      div {
                                        width: 80%;
                                      }
                                    `}</style>
                                </div>
                            </footer>
                        </div>
                        <div className={styles.chart}>
                            <span>Kubernetes</span>
                            <footer>
                                <div className={styles.chartBar}>
                                    <style jsx>{`
                                      div {
                                        width: 70%;
                                      }
                                    `}</style>
                                </div>
                            </footer>
                        </div>
                        <div className={styles.chart}>
                            <span>Docker</span>
                            <footer>
                                <div className={styles.chartBar}>
                                    <style jsx>{`
                                      div {
                                        width: 85%;
                                      }
                                    `}</style>
                                </div>
                            </footer>
                        </div>
                        <div className={styles.chart}>
                            <span>Spring</span>
                            <footer>
                                <div className={styles.chartBar}>
                                    <style jsx>{`
                                      div {
                                        width: 60%;
                                      }
                                    `}</style>
                                </div>
                            </footer>
                        </div>
                        <div className={styles.chart}>
                            <span>HTML, CSS, JS</span>
                            <footer>
                                <div className={styles.chartBar}>
                                    <style jsx>{`
                                      div {
                                        width: 50%;
                                      }
                                    `}</style>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
                <h2>Experience</h2>
                <p>In my career, I was working for the following organizations :</p>
                <div className={styles.experienceContainer}>
                    <article className={styles.experience}>
                        <h3>Axisdata - Junior Java Developer</h3>
                        <p><small>October 2017 - Present</small></p>
                    </article>
                    <article className={styles.experience}>
                        <h3>Building Company - Owner, Founder</h3>
                        <p><small>January 2016 - August 2017</small></p>
                    </article>
                </div>
            </section>
        </div>
    );
};

export default Skills;