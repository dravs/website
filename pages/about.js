import Head from "next/head";
import Link from 'next/link'
import styles from "../styles/AboutMe.module.css";

const About = () => {
    const AboutMeImg = "images/me/pic.png";
    return (
        <div>
            <Head>
                <title>Damian Prorok | Software Developer | About</title>
                <meta name="description" content="About me page"/>
            </Head>
            <section className={styles.aboutMeContainer} id="aboutMe">
                <h2>About Me</h2>
                <div className={styles.aboutMe}>
                    <div className={styles.aboutMeText}>
                        <h1>Hi, I&apos;m Damian</h1>
                        <p>I&apos;m a Software Developer from
                            Gorlice, Poland, where I live and currently working remotely for <Link
                                href={"https://axisdata.net/"}>Axisdata</Link> company. </p>
                        <p>I&apos;ve been a developer since I started my career 4 years ago.
                            I&apos;ve always liked learning new things. My passion starts at high school,
                            from game written in Java. I wanted to understand how things worked, so I started server
                            of that game, and
                            I analyzed their source code. Still love to play Lineage 2, but currently, I&apos;ve changed
                            computer games to dolls.</p>
                        <p>I married the woman I&apos;ve loved since middle-school, and we have a wonderful daughter.
                            I spend most of my spare time with them. Rest of that free time, I&apos;m spending playing
                            football, or in the gym. I am constantly working on improving my knowledge. Interested in
                            new technologies, bodybuilding and MMA.</p>
                    </div>
                    <div className={styles.aboutMeImages}>
                        <img src={AboutMeImg} alt="Damian Prorok" className={styles.image}/>
                    </div>
                </div>
            </section>
        </div>
    );
};

export default About;