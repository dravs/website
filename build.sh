#!/bin/sh

image_version="0.0.1"
image_name="website-arm"
repository=""
platform="linux/arm/v7"
push="false"

parse_args() {
  while [ "$1" ]; do
    case "$1" in
      --platform)shift;platform=$1;;
      -r|--repository)shift;repository=$1;;
      -v|--version)shift;image_version=$1;;
      -p|--push)push="true";;
      -h|--help)
                 cat <<EOT
Usage: build.sh [OPTIONS]

Options:
  --platform          Specify platform for image build (default: $platform)
  -r|--repository     Specify image docker repository (default: $repository)
  -v|--version        Specify version of the image (default: $image_version)
  -n|--name           Specify name of the image (default: $image_name)
  -p|--push           Push image to repository (default: false)
  -h|--help           Print this help information

Assumptions:
    - docker is installed.
EOT
        exit 0
        ;;
    esac
    shift
  done
}

main(){
  parse_args "$@"
  cmd="docker buildx build"
  [ -z "$platform" ] || cmd=$cmd" --platform ${platform}"
  [ -z "$repository" ] && { printf '%s\n' 'Repository must be specified!'; exit 1; }
  [ -z "$image_version" ] && { printf '%s\n' 'Image version must be specified!'; exit 1; }
  cmd=$cmd" -t ${repository}/${image_name}:${image_version}"
  $push && cmd=$cmd" --push"
 echo "$cmd"
  eval "$cmd" .
}

main "$@"