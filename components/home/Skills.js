import styles from '../../styles/components/home/Skills.module.css'
import {useInView} from "react-intersection-observer";
import Java from "../../public/svg/skills/java.svg";
import Docker from "../../public/svg/skills/docker.svg";
import K8s from "../../public/svg/skills/kubernetes.svg";
import Spring from "../../public/svg/skills/spring.svg";
import Linux from "../../public/svg/skills/linux.svg";


const Skills = () => {
    const [skillsRef, skillsInView] = useInView({
        triggerOnce: true,
    });

    return (
        <section className={styles.skillsContainer}>
            <div ref={skillsRef}>
                {skillsInView &&
                <>
                    <h2>Skills</h2>
                    <div className={styles.skills}>
                        <p className={`${styles.skill} ${styles.skillJava}`}><Java width={50} height={50} fill={'white'}/> JAVA</p>
                        <p className={`${styles.skill} ${styles.skillDocker}`}><Docker width={34} height={34} fill={'white'}/> Docker</p>
                        <p className={`${styles.skill} ${styles.skillK8s}`}><K8s width={30} height={30} fill={'white'}/> Kubernetes</p>
                        <p className={`${styles.skill} ${styles.skillLinux}`}><Linux width={24} height={24} fill={'white'}/> Linux</p>
                        <p className={`${styles.skill} ${styles.skillSpring}`}><Spring width={24} height={24} fill={'white'}/> Spring</p>
                        <p className={`${styles.skill} ${styles.skillGIT}`}>GIT</p>
                        <p className={`${styles.skill} ${styles.skillTDD}`}>TDD</p>
                        <p className={`${styles.skill} ${styles.skillDDD}`}>DDD</p>
                        <p className={`${styles.skill} ${styles.skillCICD}`}>CI/CD</p>
                        <p className={`${styles.skill} ${styles.skillMongo}`}>MongoDB</p>
                        <p className={`${styles.skill} ${styles.skillKafka}`}>Kafka</p>
                        <p className={`${styles.skill} ${styles.skillSQL}`}>SQL</p>
                        <p className={`${styles.skill} ${styles.skillHibernate}`}>Hibernate</p>
                        <p className={`${styles.skill} ${styles.skillREST}`}>REST</p>
                        <p className={`${styles.skill} ${styles.skillScripts}`}>BASH SCRIPTS</p>
                    </div>
                    <div className={styles.skillsContainerText}>
                        <p>There is more! I constantly keep working on improving my skillset. In the Dev community,
                            are a lot of great tools, which every developer should know, use it. The most important
                            is to have fun and satisfaction from learning something new.</p>
                        <p><i>&quot;An investment in knowledge pays the best interest.&quot; - Benjamin Franklin</i>
                        </p>
                    </div>
                    <a href="skills" role="button">Learn More</a></>}
            </div>
        </section>
    );
};

export default Skills;