import styles from '../../styles/components/home/Hello.module.css'
import ArrowDown from "../../public/svg/arrow.svg";

const Hello = () => {
    return (
        <section className={styles.homeWrapper}>
            <div className={styles.titles}>
                <h4 className={styles.welcomeTitle}>Hi there! I&apos;m</h4>
                <div className={styles.title}>
                    <h1>DAMIAN PROROK</h1>
                </div>
                <div className={styles.subTitle}>
                    <h4>a self-learning experience developer through research, learn and coding</h4>
                </div>
            </div>
            <div className={styles.scrollDown}>
                <button className={styles.scrollBtn} type="button">
                    <ArrowDown width={34} height={34} fill={'white'} onClick={() => location.href = '#aboutMe'}/>
                </button>
            </div>
        </section>
    );
};

export default Hello;