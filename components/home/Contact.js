import styles from '../../styles/components/home/Contact.module.css'
import {useInView} from "react-intersection-observer";
import ContactForm from "../ContactForm"

const Contact = () => {

    const [contactRef, contactInView] = useInView({
        triggerOnce: true,
    });


    return (
        <section className={styles.contactContainer}>
            <div ref={contactRef}>
                {contactInView &&
                <>
                    <h2>Contact</h2>
                    <p>Got a question or proposal, or just want to say hello? Go ahead.</p>
                    <ContactForm/>
                </>}</div>
        </section>
    );
};

export default Contact;