import styles from '../../styles/components/home/Experience.module.css'
import {useInView} from "react-intersection-observer";

const Experience = () => {
    const [experienceRef, experienceInView] = useInView({
        triggerOnce: true,
    });

    return (
        <section className={styles.experienceContainer}>
            <div ref={experienceRef}>
                {experienceInView &&
                <>
                    <h2>Experience</h2>
                    <div className={styles.experienceRow}>
                        <article className={styles.experience}>
                            <h3>Axisdata - Junior Java Developer</h3>
                            <p><small>October 2017 - Present</small></p>
                            <ul>
                                <li><p>Work with a team to move a system into K8s microservices, with in-memory
                                    database</p></li>
                                <li><p>Write projects with Domain-Driven Design and the Hexagonal Architecture</p>
                                </li>
                                <li><p>Automate development process with versioning CI/CD and Feature flags</p></li>
                            </ul>
                        </article>
                        <article className={styles.experience}>
                            <h3>Building Company - Owner, Founder</h3>
                            <p><small>January 2016 - August 2017</small></p>
                            <ul>
                                <li><p>Collaborated with management and lead a team of people, organization daily
                                    workflow, problem solving</p></li>
                                <li><p>Self-learning to change Job dramatically</p></li>
                            </ul>
                        </article>
                    </div>
                </>}
            </div>
        </section>
    );
};

export default Experience;