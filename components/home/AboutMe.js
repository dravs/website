import styles from '../../styles/components/home/AboutMe.module.css'
import {useInView} from "react-intersection-observer";

const AboutMe = () => {
    const imgCircle = "images/me/me-circle.png";
    const [aboutMeRef, aboutMeInView] = useInView({
        triggerOnce: true,
    });

    return (
        <section className={styles.aboutMeContainer} id="aboutMe">
            <div ref={aboutMeRef}>
                {aboutMeInView &&
                <>
                    <h2>About Me</h2>
                    <div className={styles.aboutMe}>
                        <div className={styles.aboutMeText}>
                            <p>Hello, My name is Damian. I&apos;m a passionate self-taught software developer from
                                Poland. I was never the best at school, but I always knew what I want to do in life.
                                I have been working in IT for the past 4 years. I always like learning new things
                                and I&apos;m happy that I can connect passion with work now.</p>
                            <p>I have many interests besides technology like Football, outdoor activities and games.
                                Loves to eat healthy and pick up a barbell with heavy plates on it.</p>
                        </div>
                        <div className={styles.aboutMeImage}>
                            <img src={imgCircle} alt="Damian Prorok image" className={styles.img}/>
                        </div>
                    </div>
                    <a href="about" role="button">Read More</a></>}</div>
        </section>
    );
};

export default AboutMe;