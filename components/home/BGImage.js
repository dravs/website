const BGImage = () => {
    const bgImage = "images/background.jpg";
    return (
        <div className="bgWrapper">
            <div className="homeImg"/>
            <style jsx>{`
              .bgWrapper {
                position: fixed;
                height: 100%;
                width: 100%;
                overflow: hidden;
                z-index: -1;
                top: 0;
                left: 0;
              }

              .homeImg {
                background-image: url(${bgImage});
                height: 100%;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
              }
            `}</style>
        </div>
    );
};

export default BGImage;