import styles from '../styles/components/MobileNavbar.module.css'
import MobileNav from '../public/svg/mobileNav.svg'
import {useState} from "react";
import Link from "next/link";
import LinkedIn from "../public/svg/linkedin.svg";
import GitLab from "../public/svg/gitlab.svg";

const MobileNavbar = () => {
    const [navOpened, setNavOpened] = useState(false);
    const closeNav = ()=>setNavOpened(false);
    return (<>
        <div className={`${styles.overlay}  ${navOpened ? '' : styles.overlayClosed}`}>
            {( navOpened && (
                <div className={styles.navbarButton}>
            <span className={styles.closeButton} onClick={()=> setNavOpened(false)}>&times;</span>
            </div>))}
            <div className={styles.overlayContent}>
                <div className={styles.home}>
                    <Link href="/"><a onClick={closeNav}>Home</a></Link>
                </div>
                <ul className={styles.navigation}>
                    <li><Link href="/about"><a onClick={closeNav}>About Me</a></Link></li>
                    <li><Link href="/skills"><a onClick={closeNav}>My Skills</a></Link></li>
                    <li><Link href="/software"><a onClick={closeNav}>Software</a></Link></li>
                    <li><Link href="/contact"><a onClick={closeNav}>Contact</a></Link></li>
                </ul>
                <ul className={styles.social}>
                    <li><Link href="https://www.linkedin.com/in/damian-prorok/"><a><LinkedIn width={18} height={18} fill={'white'}/></a></Link></li>
                    <li><Link href="https://gitlab.com/dravs"><a><GitLab width={18} height={18} fill={'white'}/></a></Link></li>
                </ul>
            </div>
        </div>
            {( !navOpened && (
                <div className={styles.navbarButton}>
        <MobileNav width={34} height={34} fill={'white'} onClick={()=>setNavOpened(true)}/></div> ))}
        </>
);
};

export default MobileNavbar;