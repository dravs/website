import styles from '../styles/components/Footer.module.css'

const Footer = () => {
    return (
        <footer className={styles.footer}>
          <p className={styles.footerText}>© 2021 Dravs</p>
        </footer>
    );
};

export default Footer;