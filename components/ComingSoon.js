import styles from '../styles/components/ComingSoon.module.css'

const ComingSoon = (props) => {
    const whatsComing = props.pagename;

    return (
        <div className={styles.comingSoonContainer}>

        <div className={styles.comingSoon}>
            <h1>{whatsComing}</h1>
            <h2>Coming soon...</h2>
        </div>
        </div>
    );
};

export default ComingSoon;