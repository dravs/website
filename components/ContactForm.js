import styles from '../styles/components/ContactForm.module.css'
import {useState, useEffect} from "react";

const ContactForm = () => {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [subject, setSubject] = useState('')
    const [message, setMessage] = useState('')
    const [submitted, setSubmitted] = useState(false)

    const handleSubmit = (e) => {
        e.preventDefault()
        console.log('Sending')
        let data = {
            name,
            email,
            subject,
            message
        }
        fetch('/api/contact', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((res) => {
            if (res.status === 200) {
                console.log('Sending message succeeded!');
                setSubmitted(true);
                setName('');
                setSubject('');
                setEmail('');
                setMessage('');
            }
        })
    }

    return (
                    <div className={styles.contact}>
                        <form className={styles.contactForm}>
                            <div className={styles.contactEmailGroup}>
                                < div className={styles.contactInputGroup}>
                                    < label htmlFor='name' className={styles.contactInputLabel}>Name</label>
                                    < input type='text' name='name' className={styles.contactInputField} value={name} onChange={(e)=>{setName(e.target.value)}}/>
                                </div>
                                < div className={styles.contactInputGroup}>
                                    < label htmlFor='email' className={styles.contactInputLabel}>Email</label>
                                    < input type='email' name='email' className={styles.contactInputField} value={email} onChange={(e)=>{setEmail(e.target.value)}}/>
                                </div>
                            </div>
                            < div className={styles.contactInputGroup}>
                                < label htmlFor='subject' className={styles.contactInputLabel}>Subject</label>
                                < input type='text' name='subject' className={styles.contactInputField} value={subject} onChange={(e)=>{setSubject(e.target.value)}}/>
                            </div>
                            < div className={styles.contactInputGroup}>
                                < label htmlFor='message' className={styles.contactInputLabel}>Message</label>
                                < textarea name='message' className={styles.contactInputField} value={message} onChange={(e)=>{setMessage(e.target.value)}}/>
                            </div>
                            < input type='submit' value="Send" className={styles.contactInputSubmit} onClick={(e)=>{handleSubmit(e)}}/>
                        </form>
                    </div>
    );
};

export default ContactForm;