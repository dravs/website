import styles from "../styles/components/Map.module.css";
import Image from "next/image";
import MapPointer from "../public/svg/map-pointer.svg";
import MapImage from "../public/images/map.png";

const Map = () => {
    return (
        <div className={styles.mapContainer}>
            <Image src={MapImage} alt="Map" quality={100} width={500}
                   height={500} objectPosition='center'/>
            <div className={styles.mapPointerContainer}>
                <p>Gorlice</p>
                <MapPointer width={24} height={24} fill={'black'} className={styles.mapPointer}/>
            </div>
        </div>
    );
};

export default Map;