import Navbar from "./Navbar";
import Footer from "./Footer";
import Head from "next/head";
import MobileNavbar from "./MobileNavbar";

const Layout = ({children}) => {
    return (
        <div className="wrapper">
            <Head>
                <title>Damian Prorok | Software Developer</title>
                <meta name="description" content="Personal website"/>
                <link rel="icon" href="/webpage.ico"/>
            </Head>
            <Navbar page={children.type.displayName ? children.type.displayName : children.type.name }/>
            <MobileNavbar/>
            <div className="content">
                {children}
            </div>
            <Footer/>
        </div>
    );
};

export default Layout;