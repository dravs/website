import Link from 'next/link'
import {useState, useEffect, useCallback} from "react";
import styles from '../styles/components/Navbar.module.css'
import LinkedIn from '../public/svg/linkedin.svg'
import GitLab from '../public/svg/gitlab.svg'

const Navbar = (props) => {
    let listener = useCallback((e) => {
        let scrolled = document.scrollingElement.scrollTop;
        if (scrolled >= 120) {
            setScrollOnTop(false)
        } else {
            setScrollOnTop(true)
        }
    }, []);
    const currentPage = props.page.toUpperCase();
    const isHomePage = currentPage === "HOME";
    const [scrollOnTop, setScrollOnTop] = useState(true);

    useEffect(() => {
        if(isHomePage) {
            document.addEventListener("scroll", listener);
            return () => {
                document.removeEventListener("scroll", listener)
            }
        }
    }, [scrollOnTop, isHomePage]);

    useEffect(() => {
        if(isHomePage) {
           setScrollOnTop(true);
        } else {
            setScrollOnTop(false);
        }
    }, [isHomePage]);

    return (
        <nav className={`${styles.navbar} ${!scrollOnTop ? styles.navbarScrolled : ''}`}>
            <div className={styles.logo}>
                <Link href="/">Home</Link>
            </div>
                <ul className={styles.navigation}>
                    <li><Link href="/about">About Me</Link></li>
                    <li><Link href="/skills">My Skills</Link></li>
                    <li><Link href="/software">Software</Link></li>
                    <li><Link href="/contact">Contact</Link></li>
                </ul>
                <ul className={styles.social}>
                    <li><Link href="https://www.linkedin.com/in/damian-prorok/"><a><LinkedIn width={18} height={18} fill={scrollOnTop?'white':'black'}/></a></Link></li>
                    <li><Link href="https://gitlab.com/dravs"><a><GitLab width={18} height={18} fill={scrollOnTop?'white':'black'}/></a></Link></li>
                </ul>
        </nav>
    );
};

export default Navbar;